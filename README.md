The MIT License (MIT)

Copyright (c) 2015 Atlassian

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

-----------------------------------------------------------------------------------

Jira in a box allows you to start a Jira version of a released version with minimum human interaction. This can be done in three different options:

* Locally in your machine (no environment control, probably faster)
* Locally in a vagrant machine (middle point)
* In a docker container, in a vagrant machine (full environment control but no access to jira files)

# Set up in local environment #
just clone the repo:

```
#!bash

mkdir -p ~/jbox/work
mkdir -p ~/jbox/script && cd $_
git clone https://bitbucket.org/atlassian/jira-box.git
``` 

# Setup jbox with Vagrant #
Create a directory that would be used to save downloaded content, this folder is on your HOST pc/mac.

```
#!bash

mkdir ~/jira_distro
```

## Create the vagrant image (skip if you want to run jira locally) ##
### Option 1: Download the base box ###
* Download jbox_vagrant_v1.box from the downloads section
* Install the vagrant image.

```
#!bash

vagrant box add --name jbox_vagrant_v1 ~/jira_distro/jbox_vagrant_v1.box
```

### Option 2: Create Vagrant instance: ###
This would create a new vagrant instance using the installed image.

```
#!bash

wget https://bitbucket.org/atlassian/jira-box/downloads/Vagrantfile
vagrant up
```

# Create JIRA instance: #
## In local environment ##
cd to your clone folder and run setup

```
#!bash

cd jira-box/jbox
./jbox setup
```

## In vagrant (either docker or local) ##
SSH to vagrant instance, set alias and run setup.

```
#!bash

vagrant ssh
alias jbox="/opt/jbox/script/jira-box/jbox"
jbox setup
```

# In local mac/linux #

```
#!bash

cd jira-box/
./jbox setup
```

# jbox util commands  (currently Docker only) #

```
#!bash

./jbox start - Launches a wizard and runs jira
./jbox status - Shows info about current running jira instances
./jbox stops N - Stops given jira instance
```