# Utility scripts #
## Package Vagrant image ##

```
#!bash

vagrant package
mv package.box jbox_vagrant_v1.box
```

# Creating Docker image #
This can be done inside the Vagrant image. Change paths if your not creating image from inside docker.
## Install Docker ##
Add GPG key

```
#!bash

sudo sh -c "wget -qO- https://get.docker.io/gpg | apt-key add -"
```

Add Repository

```
#!bash

sudo sh -c "echo deb https://get.docker.com/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
```

Install Docker

```
#!bash

sudo apt-get update
sudo apt-get install -y lxc-docker
```

## Setup Docker base ##
Create a docker working directory

```
#!bash

sudo mkdir -p /opt/jbox/docker_base && cd $_
```

Grab the Docker file - from the repo

```
#!bash

sudo cp /opt/jbox/script/jira-box/src/main/resources/vagrant/v1/Dockerfile .
```

## Build Docker image ##
Note the account user name needs to be pre-appended to image name

```
#!bash

sudo docker build -t "cnortje/java_base_v1" .
```

## Upload Docker image ##
Create account at https://hub.docker.com/account/signup/
Login

```
#!bash

sudo docker login
```

Publish - Note the account user name needs to be pre-appended to image name

```
#!bash

sudo docker push "cnortje/java_base_v1"
```

#Pull image

```
#!bash

sudo docker pull cnortje/java_base_v1
```

#Test Image

```
#!bash

sudo docker run -i -t cnortje/java_base_v1 /bin/bash
```

# Adding new JIRA Home for different JIRA Version #
Create a JIRA Home directory using this exact path /opt/jbox/jira/home
Update JIRA to use that home directory
Start JIRA and complete setup

```
#!bash

tar -czf jira_version_here_description.tar.gz /opt/jbox/jira/home
```
