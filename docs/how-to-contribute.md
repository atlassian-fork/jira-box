# How to contribute #
All jira in a box does to set up a new instance is defined in json files, starting with `src/main/resources/setup-options/data/v1/jira.json`.

It looks like this:

```json
{
  "description": "Please select JIRA version:",
  "key": "JIRA_VERSION",
  "prompt": true,
  "options": {
    "6.3.15": {
      "value": "http://nexus-atlassian-private.buildeng.atlassian.com:8081/content/repositories/atlassian-private/com/atlassian/jira/jira-standalone-distribution/6.3.15/jira-standalone-distribution-6.3.15-standalone.tar.gz",
      "link": "jira_6_3_15_jira_home.json"
    },
    "6.3.9": {
      "value": "http://nexus-atlassian-private.buildeng.atlassian.com:8081/content/repositories/atlassian-private/com/atlassian/jira/jira-standalone-distribution/6.3.9/jira-standalone-distribution-6.3.9-standalone.tar.gz",
      "link": "jira_6_3_9_jira_home.json"
    },
    "Or Just Enter Custom version": {
      "value": "http://nexus-atlassian-private.buildeng.atlassian.com:8081/content/repositories/atlassian-private/com/atlassian/jira/jira-standalone-distribution/OTHER_VERSION/jira-standalone-distribution-OTHER_VERSION-standalone.tar.gz",
      "link": "jira_all_jira_home.json"
    }
  }
}
```

Each json file has following fields:
* Description: The text that will appear in setup wizard
* key: Thee dictionary key it's going to be used to store the selected value
* prompt: If the user is going to be propted for this or if the first (usually only) option is going to be automatically selected
* options: A dictionary of options

Each option contains the following fields:
* Item name: The option the user is going to be presented with
* value: The value that's going to be stored in the config dictionary
* link: Link to next configuration json if there's any.


If you want to add more config options you will probably need to upgrade jirautil.py to handle it. This is how jirautil works:

