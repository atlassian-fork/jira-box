__author__ = 'jsimon'

import os
import shutil
import subprocess
import socket
import fileinput
import re
import sys
import getpass
from jbsetup import util, runninginfo


DEBUG = False


class JiraUtil(object):
    
    _is_local = False
    JBOX_HOME = "/opt/jbox"
    _jbox_home = JBOX_HOME

    @property
    def is_local(self):
        return self._is_local

    @property
    def is_vagrant(self):
        return getpass.getuser() == 'vagrant' or socket.gethostname() == 'precise64'

    @is_local.setter
    def is_local(self, value):
        self._is_local = value
        if value and not self.is_vagrant:
            self._jbox_home = os.path.expanduser('~/jbox')
        else:
            self._jbox_home = os.path.abspath(self.JBOX_HOME)

    @property
    def jbox_home(self):
        return self._jbox_home

    @property
    def distro(self):
        # Where JIRA zips and home zips will be downloaded
        return os.path.join(self.jbox_home, "jira_distro")

    @property
    def jira_work(self):
        # jira working directory: where JIRA zips will be extracted
        return os.path.join(self.jbox_home, "work")

    @property
    def jira_home(self):
        # jira working directory: where JIRA zips will be extracted
        return os.path.join(self.jbox_home, "jira/home")

    @property
    def jira_runtime(self):
        return os.path.join(self.jbox_home, "runtime")

    def get_jira(self, jira_url, home_url):
        """ Downloads jira and the home url and extracts them to JIRA_WORK directory
        :param jira_url: url or path relative to resources folder
        :param home_url: url or path relative to resources folder
        :return: extracted jira folder and home folder paths
        """
        jira_folder = None
        home_folder = None
        jira_zip_file = jira_url.split("/")[-1]
        jira_home_zip_file = home_url.split("/")[-1]
    
        jira_path = os.path.join(self.distro, jira_zip_file)
        if os.path.exists(jira_path):
            print 'Using JIRA distribution: ', jira_path
        else:
            print 'Downloading JIRA distribution from: ', jira_url
            jira_path = util.download(jira_url, self.distro)
    
        home_path = os.path.join(self.distro, jira_home_zip_file)
        if os.path.exists(home_path):
            print 'Using JIRA Home ZIP: ', home_path
        else:
            print 'Downloading JIRA Home ZIP from: ', home_url
            home_path = util.download(home_url, self.distro)
    
        if os.path.exists(jira_path) and os.path.getsize(jira_path) > (1024 * 1024):
            print 'Extracting JIRA distribution...',  # No newline
            jira_folder = util.extract(jira_path, self.jira_work)
            print 'Ok'
            print 'Extracted JIRA distribution in : ', jira_folder
        else:
            print 'Failed to download JIRA: ', jira_url
    
        if os.path.exists(home_path) and os.path.getsize(home_path) > (20):
            print 'Extracting JIRA Home...',  # No newline
            home_folder = util.extract(home_path, self.jira_work)
            print 'Ok'
            print 'Extracted JIRA Home in : ', home_folder
        else:
            print 'Failed to download JIRA Home: ', home_url
    
        if jira_folder is None or home_folder is None:
            sys.exit()
        return jira_folder, home_folder

    def config_jira(self, jira_folder, home_folder, jira_folder_name=None, dataset_path=None, plugins_folder=None, ):
        """ Configure jira with provided information
        :param jira_folder: Extracted jira dir
        :param home_folder: JIRA_HOME folder
        :param dataset_path:
        :param plugins_folder:
        :return:
        """
        # Set JIRA HOME
        jira_home_config = self.jira_home if jira_folder_name is None else os.path.join(self.jira_runtime,
                                                                                        os.path.join(jira_folder_name,
                                                                                                     'home'))
        with open(os.path.join(jira_folder, 'atlassian-jira/WEB-INF/classes/jira-application.properties'),
                  "w") as properties_file:
            properties_file.write("\njira.home = %s" % jira_home_config)
        print 'Configured JIRA Home to: %s' % jira_home_config
    
        print 'Moving JIRA Home from ', home_folder, " to ", jira_folder
        shutil.move(home_folder, jira_folder)

    def start_local_jira(self, jira_path):
        folder_name = jira_path.split("/")[-1]
        folder_path = os.path.join(self.jira_runtime, folder_name)
        jira_home_config = self.jira_home if folder_name is None else os.path.join(self.jira_runtime,
                                                                                   os.path.join(folder_name, 'home'))
    
        config_file = os.path.join(os.path.join(self.jira_work, folder_name), 'home/dbconfig.xml')
        if os.path.exists(config_file):
            for line in fileinput.input(config_file, inplace=True):
                print(line.replace('/opt/jbox/jira/home', str(jira_home_config)))
    
        jira_connector_port, jira_server_port = self.get_free_local_ports()

        config_file = os.path.join(os.path.join(self.jira_work, folder_name), 'conf/server.xml')
        if os.path.exists(config_file):
            for line in fileinput.input(config_file, inplace=True):
                print(line.replace('8080', str(jira_connector_port)))
        if os.path.exists(config_file):
            for line in fileinput.input(config_file, inplace=True):
                print(line.replace('8005', str(jira_server_port)))
        if self.is_vagrant:
            if os.path.exists(config_file):
                for line in fileinput.input(config_file, inplace=True):
                    print(line.replace('localhost', str(util.get_ip_address('eth1'))))
            print "JIRA Port:", jira_connector_port
        else:
            print "Server xml does not exist in ", config_file
    
        print 'Moving ', jira_path, " to ", folder_path
        if os.path.exists(folder_path):
            shutil.rmtree(folder_path)
        shutil.move(jira_path, folder_path)
        start_script = os.path.join(folder_path, 'bin/start-jira.sh')
        subprocess.Popen(start_script, shell=True)

        hostname = 'localhost'
        if self.is_vagrant:
            hostname = util.get_ip_address('eth1')
        print "JIRA Started at : http://%s:%s" % (hostname, jira_connector_port)
    
    def start_docker(self, port):
        """ Starts docker and attaches to it """
        os.environ['DOCK_PORT'] = port
        shutil.copy(util.get_resource_path('docker_start_jira.sh'), os.path.join(self.jira_work, 'docker_start_jira.sh'))
        docker_script = util.get_resource_path('start_docker.sh')
        p = subprocess.Popen(docker_script, shell=True, stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if stderr:
            print stderr
        docker_hash = stdout.rstrip()
        print "If you want to see JIRA logs you can run: "
        print "sudo docker attach %s" % docker_hash
        print "JIRA running at: http://192.168.33.10:%s" % port
        return docker_hash

    def clean_work_directory(self):
        """ Removes and recreates work dir. """
        if os.path.exists(self.jira_work):
            shutil.rmtree(self.jira_work)
        os.mkdir(self.jira_work)
    
    def get_free_local_ports(self):
        configured_ports = self.get_configured_ports()
        connector_port = None
        server_port = None
        for port in range(8080, 9984):
            if connector_port is None:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                result = sock.connect_ex(('localhost', port))
                if result != 0:
                    if port not in configured_ports:
                        connector_port = port
        for port in range(7080, 8080):
            if server_port is None:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                result = sock.connect_ex(('localhost', port))
                if result != 0:
                    if port not in configured_ports:
                        server_port = port
        return connector_port, server_port
    
    def get_configured_ports(self):
        configured_ports = set()
        for jira_instance in os.listdir(self.jira_runtime):
            used_connector_port, used_server_port = _get_configured_port(os.path.join(self.jira_runtime, jira_instance))
            if used_connector_port is not None:
                configured_ports.add(int(used_connector_port))
            if used_server_port is not None:
                configured_ports.add(int(used_server_port))
        return configured_ports.union(set(runninginfo.get_used_ports()))
        
    def has_jira_installation(self, jira_version):
        for jira_instance in os.listdir(self.jira_runtime):
            if jira_version in jira_instance:
                jira_runtime_location = os.path.join(self.jira_runtime, jira_instance)
                port = _get_configured_port(jira_runtime_location)
                return True, jira_runtime_location, port
        return False, None, None



def set_docker_java_version(version, jira_folder):
    """Configures alternative java in docker container"""
    util.replace(os.path.join(jira_folder, 'bin/start-jira.sh'), '#!/bin/bash',
                 '#!/bin/bash\nupdate-java-alternatives -s %s' % version)


def provision_local_jira():
    p = subprocess.Popen(util.get_resource_path('setup_local.bash'), shell=True, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()
    if stderr:
        print stderr
    if stdout:
        print stdout


def _get_configured_port(jira_location):
    config_file = os.path.join(jira_location, 'conf/server.xml')
    connector_port = None
    server_port = None
    if os.path.exists(config_file):
        for line in fileinput.input(config_file, inplace=False):
            if connector_port is None and "<Connector" in line:
                matcher = re.search('port="(.+?)"', line)
                if matcher:
                    connector_port = matcher.group(1)
            if server_port is None and "<Server" in line:
                matcher = re.search('port="(.+?)"', line)
                if matcher:
                    server_port = matcher.group(1)
        fileinput.close()
    if DEBUG:
        print '%s has Connector port %s and Server port %s' % (config_file, connector_port, server_port)
    return connector_port, server_port 