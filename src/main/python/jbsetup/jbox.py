import sys


if __name__ == "__main__":
    from jbsetup.v1 import wizardv1 as wizard
    from jbsetup.v1 import statusv1 as status
    from jbsetup.v1 import stopv1 as stop
    
    valid_options = {'setup': wizard.main, 'status': status.main, 'stop': stop.main}
    if len(sys.argv) < 2 or sys.argv[1] not in valid_options.keys():
        print 'You need to provide an option from %s' % valid_options.keys()
    else:
        valid_options[sys.argv[1]](sys.argv[2:])