__author__ = 'atlassian'

import os

from jbsetup import util, runninginfo
from jbsetup.jirautil import JiraUtil


class Wizard(object):
    
    def __init__(self):
        self.jirautil = JiraUtil()

    def get_config(self):
        setup_options = {}
        json_file = 'runtime.json'
    
        while json_file:
            file_options = util.load_json_file(json_file)
            custom_value = None
    
            if _should_select_default_java(setup_options, file_options):
                selected_option_key = file_options['options'].keys()[0]
            elif file_options['prompt']:
                if file_options['key'] == "JIRA_VERSION":
                    custom_value, selected_option_key = _get_jira_version(file_options)
                    setup_options['JIRA_VERSION_NUM'] = custom_value or selected_option_key
                else:
                    selection, is_custom = util.query_config(file_options['description'], file_options['options'].keys())
                    selected_option_key = unicode(selection)
            else:
                selected_option_key = file_options['options'].keys()[0]
    
            selected_option = file_options['options'][selected_option_key]
            
            if custom_value is not None:
                setup_options[str(file_options['key'])] = str(selected_option['value']).replace("OTHER_VERSION",
                                                                                                custom_value)
            else:
                setup_options[str(file_options['key'])] = selected_option['value']
    
            if _is_local(file_options, setup_options):
                self.jirautil.is_local = True
                _setup_local()
    
            json_file = selected_option.get('link', None)
        return setup_options

    def apply_config(self, setup_options):
        if setup_options['JBOX_RUNTIME'] == "LOCAL":
            jira_already_running, jira_runtime_location, port_info = self.jirautil.has_jira_installation(
                setup_options['JIRA_VERSION_NUM'])
            if jira_already_running:
                print "You already have a JIRA %s instance at %s using ports %s" % (
                    setup_options['JIRA_VERSION_NUM'], jira_runtime_location, port_info)
                print 'http://localhost:%s' % port_info[0]
                return
    
        self.jirautil.clean_work_directory()
        jira_path, home_path = self.jirautil.get_jira(setup_options['JIRA_VERSION'], setup_options['JIRA_HOME_DATA'])
    
        jira_folder_name = jira_path.split("/")[-1] if setup_options['JBOX_RUNTIME'] == "LOCAL" else None
        self.jirautil.config_jira(jira_path, home_path, jira_folder_name)
        
        if setup_options['JBOX_RUNTIME'] == "DOCKER":
            self._start_docker(jira_path, setup_options)
        if setup_options['JBOX_RUNTIME'] == "LOCAL":
            self.jirautil.start_local_jira(jira_path)

    def _start_docker(self, jira_path, setup_options):
        print 'Configuring java version... ',  # No newline
        from jbsetup import jirautil
        jirautil.set_docker_java_version(setup_options['JAVA_VERSION'], jira_path)
        print 'Ok'
        print 'Starting docker container...'
        jira_connector_port, jira_server_port = self.jirautil.get_free_local_ports()
        docker_hash = self.jirautil.start_docker(str(jira_connector_port))
        runninginfo.add_running_instance(setup_options, docker_hash, str(jira_connector_port))

    
def _should_select_default_java(setup_options, file_options):
    return 'JBOX_RUNTIME' in setup_options and setup_options['JBOX_RUNTIME'] == "LOCAL" and file_options['key'] == "JAVA_VERSION"


def _get_jira_version(file_options):
    selection, is_custom = util.query_config(file_options['description'],
                                             file_options['options'].keys(), True)
    custom_value = None
    if is_custom:
        custom_value = selection
        for key in file_options['options'].keys():
            if "Enter" in key:
                selected_option_key = key
    else:
        selected_option_key = unicode(selection)
    return custom_value, selected_option_key


def _is_local(file_options, setup_options):
    return file_options['prompt'] and file_options['key'] == "JBOX_RUNTIME" and setup_options['JBOX_RUNTIME'] == "LOCAL"


def _setup_local():
    JBOX_PATH = os.path.expanduser("~/jbox")
    distro = os.path.expanduser("~/jira_distro")
    distro_link = os.path.join(JBOX_PATH, 'jira_distro')
    runtime = os.path.join(JBOX_PATH, 'runtime')
    work = os.path.join(JBOX_PATH, 'work')

    if not os.path.exists(work):
        os.makedirs(work)

    if not os.path.exists(distro):
        os.mkdir(distro)

    if not os.path.exists(distro_link):
        os.symlink(distro, distro_link)

    if not os.path.exists(runtime):
        os.mkdir(runtime)


def main(*args):
    wizard = Wizard()
    setup_options = wizard.get_config()
    wizard.apply_config(setup_options)


if __name__ == '__main__':
    main()