__author__ = 'jsimon'
import subprocess

from jbsetup import runninginfo


def main(argv):
    n = int(argv[0])
    docker_hash = runninginfo.get_docker_hash(n)
    print 'sudo docker kill %s' % docker_hash
    p = subprocess.Popen('docker kill %s' % docker_hash, shell=True)
    p.communicate()
    runninginfo.remove(n)