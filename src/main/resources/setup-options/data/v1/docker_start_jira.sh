#!/bin/sh
mkdir /opt/jbox/jira
cp -R /opt/jbox/work/atlassian-jira-*-standalone/* /opt/jbox/jira/
cd /opt/jbox/jira
sh bin/start-jira.sh
tail -f logs/*