# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "hashicorp/precise64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "~/jira_distro/", "/opt/jbox/jira_distro"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false

    # Customize the amount of memory on the VM:
    vb.memory = "2024"
  end

$script = <<SCRIPT
sudo sh -c "wget -qO- https://get.docker.io/gpg | apt-key add -"
sudo sh -c "echo deb https://get.docker.com/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
sudo apt-get update
sudo apt-get install -y lxc-docker
sudo apt-get install -y lxc-docker git python-pip vim
sudo pip install tabulate

sudo echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | sudo tee /etc/apt/sources.list.d/webupd8team-java.list
sudo echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | sudo tee -a /etc/apt/sources.list.d/webupd8team-java.list
sudo DEBIAN_FRONTEND=noninteractive apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886

sudo DEBIAN_FRONTEND=noninteractive apt-get update

sudo DEBIAN_FRONTEND=noninteractive apt-get install -y vim
sudo DEBIAN_FRONTEND=noninteractive apt-get install --fix-missing -y unzip
sudo DEBIAN_FRONTEND=noninteractive apt-get install --fix-missing -y openjdk-6-jre-headless
sudo DEBIAN_FRONTEND=noninteractive apt-get install --fix-missing -y openjdk-7-jre-headless
sudo DEBIAN_FRONTEND=noninteractive apt-get install --fix-missing -y python-software-properties
sudo echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo DEBIAN_FRONTEND=noninteractive apt-get install --fix-missing -y oracle-java6-installer
sudo DEBIAN_FRONTEND=noninteractive apt-get install --fix-missing -y oracle-java7-installer
sudo DEBIAN_FRONTEND=noninteractive apt-get install --fix-missing -y oracle-java8-installer
sudo DEBIAN_FRONTEND=noninteractive apt-get install --fix-missing -y git

sudo mkdir /opt/jbox
sudo mkdir /opt/jbox/script
sudo mkdir /opt/jbox/work
sudo mkdir /opt/jbox/runtime
cd /opt/jbox/script
sudo git clone https://bitbucket.org/atlassian/jira-box.git
sudo docker pull cnortje/java_base_v1
sudo chown vagrant:vagrant -R /opt/jbox/
alias jbox="/opt/jbox/script/jira-box/jbox"
SCRIPT

    config.vm.provision :shell, :inline => $script

end